package com.eshopping.orderservice.service;

import com.eshopping.orderservice.dto.OrderLineItemsDto;
import com.eshopping.orderservice.dto.OrderRequest;
import com.eshopping.orderservice.model.Order;
import com.eshopping.orderservice.model.OrderLineItems;
import com.eshopping.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    public void placeOrder(OrderRequest orderRequest){
        Order order =new Order();
        order.setOrderNumber(UUID.randomUUID().toString());

        List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDtos().stream()
                .map(this::maptoDto).toList();
        order.setOrderLineItemsList(orderLineItems);
        orderRepository.save(order);
    }

    private OrderLineItems maptoDto(OrderLineItemsDto orderLineItemsDto) {
     OrderLineItems orderLineItems=new OrderLineItems();
     orderLineItems.setId(orderLineItemsDto.getId());
     orderLineItems.setQuantity(orderLineItemsDto.getQuantity());
     orderLineItems.setPrice(orderLineItemsDto.getPrice());
     orderLineItems.setSkuCode(orderLineItemsDto.getSkuCode());
     return orderLineItems;

    }
}
